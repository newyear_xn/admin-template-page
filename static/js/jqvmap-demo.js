'use strict';

// jQuery Vector Map Demo
// =============================================================

var jqvMapDemo = {
  init: function init() {

    this.bindUIActions();
  },
  bindUIActions: function bindUIActions() {
    toastr.options.closeButton = true;
    toastr.options.progressBar = true;
    toastr.options.positionClass = 'toast-bottom-right';

    // event handlers
    this.handleWorld();
    this.handleUSA();
    this.handleEurope();
    this.handleGermany();
  },
  colors: function colors() {
    return {
      red: '#B76BA3',
      orange: '#EC935E',
      yellow: '#F7C46C',
      green: '#A7C796',
      teal: '#00A28A',
      blue: '#346CB0',
      purple: '#5F4B8B',
      gray: '#BEC1C4',
      grayLight: '#D4D5D7',
      grayLighter: '#F5F5F5'
    };
  },
  getColor: function getColor(color) {
    return this.colors()[color];
  },
  handleWorld: function handleWorld() {
    var sample_data = { 'af': 'http://uselooper.com/assets/javascript/pages/16.63', 'al': 'http://uselooper.com/assets/javascript/pages/11.58', 'dz': 'http://uselooper.com/assets/javascript/pages/158.97', 'ao': 'http://uselooper.com/assets/javascript/pages/85.81', 'ag': '1.1', 'ar': 'http://uselooper.com/assets/javascript/pages/351.02', 'am': 'http://uselooper.com/assets/javascript/pages/8.83', 'au': 'http://uselooper.com/assets/javascript/pages/1219.72', 'at': 'http://uselooper.com/assets/javascript/pages/366.26', 'az': 'http://uselooper.com/assets/javascript/pages/52.17', 'bs': 'http://uselooper.com/assets/javascript/pages/7.54', 'bh': 'http://uselooper.com/assets/javascript/pages/21.73', 'bd': '105.4', 'bb': 'http://uselooper.com/assets/javascript/pages/3.96', 'by': 'http://uselooper.com/assets/javascript/pages/52.89', 'be': 'http://uselooper.com/assets/javascript/pages/461.33', 'bz': 'http://uselooper.com/assets/javascript/pages/1.43', 'bj': 'http://uselooper.com/assets/javascript/pages/6.49', 'bt': '1.4', 'bo': 'http://uselooper.com/assets/javascript/pages/19.18', 'ba': '16.2', 'bw': '12.5', 'br': 'http://uselooper.com/assets/javascript/pages/2023.53', 'bn': 'http://uselooper.com/assets/javascript/pages/11.96', 'bg': 'http://uselooper.com/assets/javascript/pages/44.84', 'bf': 'http://uselooper.com/assets/javascript/pages/8.67', 'bi': 'http://uselooper.com/assets/javascript/pages/1.47', 'kh': 'http://uselooper.com/assets/javascript/pages/11.36', 'cm': 'http://uselooper.com/assets/javascript/pages/21.88', 'ca': 'http://uselooper.com/assets/javascript/pages/1563.66', 'cv': 'http://uselooper.com/assets/javascript/pages/1.57', 'cf': 'http://uselooper.com/assets/javascript/pages/2.11', 'td': 'http://uselooper.com/assets/javascript/pages/7.59', 'cl': 'http://uselooper.com/assets/javascript/pages/199.18', 'cn': 'http://uselooper.com/assets/javascript/pages/5745.13', 'co': 'http://uselooper.com/assets/javascript/pages/283.11', 'km': 'http://uselooper.com/assets/javascript/pages/0.56', 'cd': '12.6', 'cg': 'http://uselooper.com/assets/javascript/pages/11.88', 'cr': 'http://uselooper.com/assets/javascript/pages/35.02', 'ci': 'http://uselooper.com/assets/javascript/pages/22.38', 'hr': 'http://uselooper.com/assets/javascript/pages/59.92', 'cy': 'http://uselooper.com/assets/javascript/pages/22.75', 'cz': 'http://uselooper.com/assets/javascript/pages/195.23', 'dk': 'http://uselooper.com/assets/javascript/pages/304.56', 'dj': 'http://uselooper.com/assets/javascript/pages/1.14', 'dm': 'http://uselooper.com/assets/javascript/pages/0.38', 'do': 'http://uselooper.com/assets/javascript/pages/50.87', 'ec': 'http://uselooper.com/assets/javascript/pages/61.49', 'eg': 'http://uselooper.com/assets/javascript/pages/216.83', 'sv': '21.8', 'gq': 'http://uselooper.com/assets/javascript/pages/14.55', 'er': 'http://uselooper.com/assets/javascript/pages/2.25', 'ee': 'http://uselooper.com/assets/javascript/pages/19.22', 'et': 'http://uselooper.com/assets/javascript/pages/30.94', 'fj': 'http://uselooper.com/assets/javascript/pages/3.15', 'fi': 'http://uselooper.com/assets/javascript/pages/231.98', 'fr': 'http://uselooper.com/assets/javascript/pages/2555.44', 'ga': 'http://uselooper.com/assets/javascript/pages/12.56', 'gm': 'http://uselooper.com/assets/javascript/pages/1.04', 'ge': 'http://uselooper.com/assets/javascript/pages/11.23', 'de': '3305.9', 'gh': 'http://uselooper.com/assets/javascript/pages/18.06', 'gr': 'http://uselooper.com/assets/javascript/pages/305.01', 'gd': 'http://uselooper.com/assets/javascript/pages/0.65', 'gt': 'http://uselooper.com/assets/javascript/pages/40.77', 'gn': 'http://uselooper.com/assets/javascript/pages/4.34', 'gw': 'http://uselooper.com/assets/javascript/pages/0.83', 'gy': '2.2', 'ht': '6.5', 'hn': 'http://uselooper.com/assets/javascript/pages/15.34', 'hk': 'http://uselooper.com/assets/javascript/pages/226.49', 'hu': 'http://uselooper.com/assets/javascript/pages/132.28', 'is': 'http://uselooper.com/assets/javascript/pages/12.77', 'in': 'http://uselooper.com/assets/javascript/pages/1430.02', 'id': 'http://uselooper.com/assets/javascript/pages/695.06', 'ir': '337.9', 'iq': 'http://uselooper.com/assets/javascript/pages/84.14', 'ie': 'http://uselooper.com/assets/javascript/pages/204.14', 'il': 'http://uselooper.com/assets/javascript/pages/201.25', 'it': 'http://uselooper.com/assets/javascript/pages/2036.69', 'jm': 'http://uselooper.com/assets/javascript/pages/13.74', 'jp': '5390.9', 'jo': 'http://uselooper.com/assets/javascript/pages/27.13', 'kz': 'http://uselooper.com/assets/javascript/pages/129.76', 'ke': 'http://uselooper.com/assets/javascript/pages/32.42', 'ki': 'http://uselooper.com/assets/javascript/pages/0.15', 'kr': 'http://uselooper.com/assets/javascript/pages/986.26', 'undefined': 'http://uselooper.com/assets/javascript/pages/5.73', 'kw': 'http://uselooper.com/assets/javascript/pages/117.32', 'kg': 'http://uselooper.com/assets/javascript/pages/4.44', 'la': 'http://uselooper.com/assets/javascript/pages/6.34', 'lv': 'http://uselooper.com/assets/javascript/pages/23.39', 'lb': 'http://uselooper.com/assets/javascript/pages/39.15', 'ls': '1.8', 'lr': 'http://uselooper.com/assets/javascript/pages/0.98', 'ly': 'http://uselooper.com/assets/javascript/pages/77.91', 'lt': 'http://uselooper.com/assets/javascript/pages/35.73', 'lu': 'http://uselooper.com/assets/javascript/pages/52.43', 'mk': 'http://uselooper.com/assets/javascript/pages/9.58', 'mg': 'http://uselooper.com/assets/javascript/pages/8.33', 'mw': 'http://uselooper.com/assets/javascript/pages/5.04', 'my': 'http://uselooper.com/assets/javascript/pages/218.95', 'mv': 'http://uselooper.com/assets/javascript/pages/1.43', 'ml': 'http://uselooper.com/assets/javascript/pages/9.08', 'mt': '7.8', 'mr': 'http://uselooper.com/assets/javascript/pages/3.49', 'mu': 'http://uselooper.com/assets/javascript/pages/9.43', 'mx': 'http://uselooper.com/assets/javascript/pages/1004.04', 'md': 'http://uselooper.com/assets/javascript/pages/5.36', 'mn': 'http://uselooper.com/assets/javascript/pages/5.81', 'me': 'http://uselooper.com/assets/javascript/pages/3.88', 'ma': '91.7', 'mz': 'http://uselooper.com/assets/javascript/pages/10.21', 'mm': 'http://uselooper.com/assets/javascript/pages/35.65', 'na': 'http://uselooper.com/assets/javascript/pages/11.45', 'np': 'http://uselooper.com/assets/javascript/pages/15.11', 'nl': 'http://uselooper.com/assets/javascript/pages/770.31', 'nz': '138', 'ni': 'http://uselooper.com/assets/javascript/pages/6.38', 'ne': '5.6', 'ng': 'http://uselooper.com/assets/javascript/pages/206.66', 'no': 'http://uselooper.com/assets/javascript/pages/413.51', 'om': 'http://uselooper.com/assets/javascript/pages/53.78', 'pk': 'http://uselooper.com/assets/javascript/pages/174.79', 'pa': '27.2', 'pg': 'http://uselooper.com/assets/javascript/pages/8.81', 'py': 'http://uselooper.com/assets/javascript/pages/17.17', 'pe': 'http://uselooper.com/assets/javascript/pages/153.55', 'ph': 'http://uselooper.com/assets/javascript/pages/189.06', 'pl': 'http://uselooper.com/assets/javascript/pages/438.88', 'pt': '223.7', 'qa': 'http://uselooper.com/assets/javascript/pages/126.52', 'ro': 'http://uselooper.com/assets/javascript/pages/158.39', 'ru': 'http://uselooper.com/assets/javascript/pages/1476.91', 'rw': 'http://uselooper.com/assets/javascript/pages/5.69', 'ws': 'http://uselooper.com/assets/javascript/pages/0.55', 'st': 'http://uselooper.com/assets/javascript/pages/0.19', 'sa': 'http://uselooper.com/assets/javascript/pages/434.44', 'sn': 'http://uselooper.com/assets/javascript/pages/12.66', 'rs': 'http://uselooper.com/assets/javascript/pages/38.92', 'sc': 'http://uselooper.com/assets/javascript/pages/0.92', 'sl': '1.9', 'sg': 'http://uselooper.com/assets/javascript/pages/217.38', 'sk': 'http://uselooper.com/assets/javascript/pages/86.26', 'si': 'http://uselooper.com/assets/javascript/pages/46.44', 'sb': 'http://uselooper.com/assets/javascript/pages/0.67', 'za': 'http://uselooper.com/assets/javascript/pages/354.41', 'es': 'http://uselooper.com/assets/javascript/pages/1374.78', 'lk': 'http://uselooper.com/assets/javascript/pages/48.24', 'kn': 'http://uselooper.com/assets/javascript/pages/0.56', 'lc': '1', 'vc': 'http://uselooper.com/assets/javascript/pages/0.58', 'sd': 'http://uselooper.com/assets/javascript/pages/65.93', 'sr': '3.3', 'sz': 'http://uselooper.com/assets/javascript/pages/3.17', 'se': 'http://uselooper.com/assets/javascript/pages/444.59', 'ch': 'http://uselooper.com/assets/javascript/pages/522.44', 'sy': 'http://uselooper.com/assets/javascript/pages/59.63', 'tw': 'http://uselooper.com/assets/javascript/pages/426.98', 'tj': 'http://uselooper.com/assets/javascript/pages/5.58', 'tz': 'http://uselooper.com/assets/javascript/pages/22.43', 'th': 'http://uselooper.com/assets/javascript/pages/312.61', 'tl': 'http://uselooper.com/assets/javascript/pages/0.62', 'tg': 'http://uselooper.com/assets/javascript/pages/3.07', 'to': '0.3', 'tt': '21.2', 'tn': 'http://uselooper.com/assets/javascript/pages/43.86', 'tr': 'http://uselooper.com/assets/javascript/pages/729.05', 'tm': 0, 'ug': 'http://uselooper.com/assets/javascript/pages/17.12', 'ua': 'http://uselooper.com/assets/javascript/pages/136.56', 'ae': 'http://uselooper.com/assets/javascript/pages/239.65', 'gb': 'http://uselooper.com/assets/javascript/pages/2258.57', 'us': 'http://uselooper.com/assets/javascript/pages/14624.18', 'uy': 'http://uselooper.com/assets/javascript/pages/40.71', 'uz': 'http://uselooper.com/assets/javascript/pages/37.72', 'vu': 'http://uselooper.com/assets/javascript/pages/0.72', 've': 'http://uselooper.com/assets/javascript/pages/285.21', 'vn': 'http://uselooper.com/assets/javascript/pages/101.99', 'ye': 'http://uselooper.com/assets/javascript/pages/30.02', 'zm': 'http://uselooper.com/assets/javascript/pages/15.69', 'zw': 'http://uselooper.com/assets/javascript/pages/5.57' };

    $('#vmap-world').vectorMap({
      map: 'world_en',
      backgroundColor: '#ffffff',
      color: this.getColor('gray'),
      hoverOpacity: 0.7,
      selectedColor: this.getColor('red'),
      enableZoom: true,
      showTooltip: true,
      values: sample_data,
      scaleColors: [this.getColor('teal'), this.getColor('green')],
      normalizeFunction: 'polynomial',
      onRegionClick: function onRegionClick(element, code, region) {
        var message = 'You clicked ' + region + ' which has the code: ' + code.toUpperCase();
        toastr.remove();
        toastr.info(message);
      }
    });
  },
  handleUSA: function handleUSA() {
    $('#vmap-usa').vectorMap({
      map: 'usa_en',
      backgroundColor: null,
      color: this.getColor('teal'),
      enableZoom: true,
      showTooltip: true,
      selectedColor: null,
      hoverColor: null,
      colors: {
        mo: this.getColor('yellow'),
        fl: this.getColor('yellow'),
        or: this.getColor('yellow')
      },
      onRegionClick: function onRegionClick(e, code, region) {
        e.preventDefault();
        var message = 'You clicked ' + region + ' which has the code: ' + code.toUpperCase();
        toastr.remove();
        toastr.info(message);
      }
    });
  },
  handleEurope: function handleEurope() {
    $('#vmap-europe').vectorMap({
      map: 'europe_en',
      backgroundColor: null,
      color: this.getColor('teal'),
      hoverColor: this.getColor('yellow'),
      enableZoom: false,
      showTooltip: false,
      onRegionClick: function onRegionClick(element, code, region) {
        var message = 'You clicked ' + region + ' which has the code: ' + code.toUpperCase();
        toastr.remove();
        toastr.info(message);
      }
    });
  },
  handleGermany: function handleGermany() {
    $('#vmap-germany').vectorMap({
      map: 'germany_en',
      backgroundColor: null,
      color: this.getColor('teal'),
      hoverColor: this.getColor('green'),
      onRegionClick: function onRegionClick(element, code, region) {
        var message = 'You clicked ' + region + ' which has the code: ' + code.toUpperCase();
        toastr.remove();
        toastr.info(message);
      }
    });
  }
};

jqvMapDemo.init();